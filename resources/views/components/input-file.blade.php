<div class="form-group @if(!isset($single)) mb-3 @endif" id="container_{{ $id }}">

    @isset($label)
        <label for="{{ $id }}">{{ $label }} @isset($required) <span class="text-danger">*</span> @endisset @isset($helper) <i class="fa-solid fa-circle-question" data-toggle="tooltip" data-placement="top" title="{{ $helper }}"></i> @endisset</label>
    @endisset

    <input
        type="file"
        class="dropify"
        name="{{ $name }}" id="{{ $id }}"
        data-height="{{ $height }}"
        @isset($value) data-default-file="{{ $value }}" @endisset
        @isset($accept) data-allowed-file-extensions="{{ $accept }}" @endisset
        @isset($disabled) disabled @endisset
        @isset($required) @isset($requiredMarkOnly) @else required @endisset @endisset
        data-parsley-trigger="blur"
        data-parsley-errors-container="#container_{{ $id }}"
    />

</div>
