<div class="table-responsive">
    @if (session()->has('success'))
        <div class="alert alert-outline-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <div class="alert-icon">
                <i class="fa fa-check"></i>
            </div>
            <div class="alert-message">
                <span><strong>Success!</strong> {{ session()->get('success') }}</span>
            </div>
        </div>
    @endif
    @if (session()->has('error'))
        <div class="alert alert-outline-danger alert-dismissible mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <div class="alert-icon">
                <i class="fa fa-exclamation-triangle"></i>
            </div>
            <div class="alert-message">
                <span>
                    <strong>Danger!</strong> {{ session()->get('error') }}</span>
            </div>
        </div>
    @endif
    <div id="datatable-btn-container">

    </div>
    <table id="{{ $id }}" class="table table-borderless table-striped table-hover table-sm">
        <thead>
            {!! $head !!}
        </thead>
        @isset($body)
            <tbody>
                {!! $body !!}
            </tbody>
        @endisset
        @isset($foot)
            <tfoot>
                {!! $foot !!}
            </tfoot>
        @endisset
    </table>
</div>
