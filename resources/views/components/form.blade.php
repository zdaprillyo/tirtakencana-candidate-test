<form {!! $attributes !!}
    action="{{ $action }}"
    method="@if($method !== 'get'){{ 'post' }}@else{{ 'get' }}@endif"
    @isset($sendFile) enctype="multipart/form-data" @endisset
>
    @if($method !== 'get')
        @method($method)
        @csrf
    @endif

    {!! $slot !!}

    @isset($submit)
        <button type="submit" class="btn btn-{{ $submitType }} @if(!isset($submitSmall)) w-100 @endif"><i class="{{ $submitIcon }} mr-2"></i>{{ $submit }}</button>
    @endisset
</form>
