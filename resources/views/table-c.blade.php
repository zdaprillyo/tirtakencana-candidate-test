@extends('layouts.app')

@section('css')
    <link href="{{ asset('custom/css/my.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/plugins/fancybox/css/jquery.fancybox.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('custom/css/buttons.dataTables.min.css') }}" rel="stylesheet"/>
@endsection

@section('breadcrumb')
<!-- Breadcrumb-->
<div class="row pt-2 pb-2">
  <div class="col-sm-9">
      <h4 class="page-title">Tabel C</h4>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javaScript:void();">Dashtreme</a></li>
        <li class="breadcrumb-item active" aria-current="page">Tabel C</li>
      </ol>
  </div>
</div>
<!-- End Breadcrumb-->
@endsection

@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header text-uppercase">
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_add"><i class="zmdi zmdi-plus"></i> Tambah</button>
        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal_import"><i class="zmdi zmdi-plus"></i> Import</button>
      </div>
        <div class="card-body">
          <x-table id="datatable">
            @slot('head')
              <tr>
                <th>No.</th>
                <th>Kode Toko</th>
                <th>Area Sales</th>
                <th>Aksi</th>
              </tr>
            @endslot
          </x-table>
        </div>
    </div>
  </div>
</div>
@endsection

@section('modal')
    <x-modal id="modal_add" header="Form Tambah">
        <x-form action="" method="post" submit="Simpan" id="form_add">
            <x-input id="add_kode_toko" name="add_kode_toko" label="Kode Toko" placeholder="Kode Toko"/>
            <x-input id="add_area_sales" name="add_area_sales" label="Area Sales" placeholder="Area Sales"/>
        </x-form>
    </x-modal>
    <x-modal id="modal_edit" header="Form Edit">
        <x-form action="" method="put" submit="Update" id="form_edit">
            <x-input id="edit_kode_toko" name="edit_kode_toko" label="Kode Toko" placeholder="Kode Toko"/>
            <x-input id="edit_area_sales" name="edit_area_sales" label="Area Sales" placeholder="Area Sales"/>
        </x-form>
    </x-modal>
    <x-modal id="modal_import" header="Modal Import">
        <x-form action="{{ route('tabel-c.import') }}" method="post" submit="Import" id="form_import" sendFile>
            <x-input-file id="file" name="file" label="File" accept="xls xlsx" required />
        </x-form>
    </x-modal>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('custom/js/datatables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/fancybox/js/jquery.fancybox.min.js') }}"></script>
<script src="{{ asset('custom/js/sweetalert2@11.js') }}"></script>
<script src="{{ asset('custom/js/my.js') }}"></script>
<script src="{{ asset('custom/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('custom/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('custom/js/jszip.min.js') }}"></script>
<script src="{{ asset('custom/js/pdfmake.min.js') }}"></script>
<script src="{{ asset('custom/js/vfs_fonts.js') }}"></script>
<script>
    let index_url = "{{ route('tabel-c.index') }}"
    let store_url = "{{ route('tabel-c.store') }}"
    let update_url = "{{ route('tabel-c.update','-id-') }}"
    let delete_url = "{{ route('tabel-c.destroy','-id-') }}"
    let using_ajax_get_request = 1

    let columns = [
        { data: 'kode_toko' },
        { data: 'area_sales' },
    ]

    function show_detail(data) {
        $('#edit_kode_toko').val(data.kode_toko)
        $('#edit_area_sales').val(data.area_sales)
    }
    $(document).ready(function(){
        let table = generate_datatable('Tabel C',index_url,columns,[0, 'asc'],[0,1,2])
        generate_update_listener('form_edit',table)
        generate_store_listener('form_add',store_url,table)
        generate_delete_listener(delete_url,table)
        generate_show_listener(update_url,using_ajax_get_request)
    })
</script>
@endpush
