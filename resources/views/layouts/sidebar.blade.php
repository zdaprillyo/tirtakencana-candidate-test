 <!--Start sidebar-wrapper-->
 <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
<div class="brand-logo">
  <a href="index.html">
    <img src="assets/images/logo-icon.png" class="logo-icon" alt="logo icon">
    <h5 class="logo-text">Dashtreme Admin</h5>
  </a>
</div>
  <ul class="sidebar-menu">
     <li class="sidebar-header">MAIN NAVIGATION</li>
      <li>
        <a href="{{ route('tabel-a.index') }}" class="waves-effect">
          <i class="zmdi zmdi-calendar-check"></i> <span>Tabel A</span>
        </a>
      </li>
      <li>
        <a href="{{ route('tabel-b.index') }}" class="waves-effect">
          <i class="zmdi zmdi-calendar-check"></i> <span>Tabel B</span>
        </a>
      </li>
      <li>
        <a href="{{ route('tabel-c.index') }}" class="waves-effect">
          <i class="zmdi zmdi-calendar-check"></i> <span>Tabel C</span>
        </a>
      </li>
      <li>
        <a href="{{ route('tabel-d.index') }}" class="waves-effect">
          <i class="zmdi zmdi-calendar-check"></i> <span>Tabel D</span>
        </a>
      </li>
   </ul>
  </div>
  <!--End sidebar-wrapper-->
