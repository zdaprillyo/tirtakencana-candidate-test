<?php

namespace Database\Seeders;

use App\Models\TabelC;
use Illuminate\Database\Seeder;

class TabelCSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = (object)[
            ['kode_toko' => 1,'area_sales' => 'A'],
            ['kode_toko' => 2,'area_sales' => 'A'],
            ['kode_toko' => 4,'area_sales' => 'A'],
            ['kode_toko' => 6,'area_sales' => 'B'],
            ['kode_toko' => 7,'area_sales' => 'B'],
        ];
        foreach ($datas as $data) {
            TabelC::create($data);
        }
    }
}
