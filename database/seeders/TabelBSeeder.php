<?php

namespace Database\Seeders;

use App\Models\TabelB;
use Illuminate\Database\Seeder;

class TabelBSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = (object)[
            ['kode_toko' => 1,'nominal_transaksi' => 1000],
            ['kode_toko' => 2,'nominal_transaksi' => 1000],
            ['kode_toko' => 4,'nominal_transaksi' => 1000],
            ['kode_toko' => 6,'nominal_transaksi' => 1000],
            ['kode_toko' => 7,'nominal_transaksi' => 1000],
        ];
        foreach ($datas as $data) {
            TabelB::create($data);
        }
    }
}
