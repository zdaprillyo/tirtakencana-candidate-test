<?php

namespace Database\Seeders;

use App\Models\TabelD;
use Illuminate\Database\Seeder;

class TabelDSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = (object)[
            ['kode_sales' => 'A1','nama_sales' => 'Alpha'],
            ['kode_sales' => 'A2','nama_sales' => 'Blue'],
            ['kode_sales' => 'A3','nama_sales' => 'Charlie'],
            ['kode_sales' => 'B1','nama_sales' => 'Delta'],
            ['kode_sales' => 'B2','nama_sales' => 'Echo'],
        ];
        foreach ($datas as $data) {
            TabelD::create($data);
        }
    }
}
