<?php

namespace Database\Seeders;

use App\Models\TabelA;
use Illuminate\Database\Seeder;

class TabelASeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = (object)[
            ['kode_baru' => 1,'kode_lama' => 6],
            ['kode_baru' => 2,'kode_lama' => null],
            ['kode_baru' => 3,'kode_lama' => 7],
            ['kode_baru' => 4,'kode_lama' => 9],
            ['kode_baru' => 5,'kode_lama' => 8],
        ];
        foreach ($datas as $data) {
            TabelA::create($data);
        }
    }
}
