<?php

namespace Database\Seeders;

use App\Models\TabelA;
use App\Models\TabelB;
use App\Models\TabelC;
use App\Models\TabelD;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        TabelA::truncate();
        TabelB::truncate();
        TabelC::truncate();
        TabelD::truncate();
        Schema::enableForeignKeyConstraints();
        $this->call([
            TabelASeeder::class,
            TabelBSeeder::class,
            TabelCSeeder::class,
            TabelDSeeder::class,
        ]);
    }
}
