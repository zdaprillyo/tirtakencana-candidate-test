<?php

namespace App\Http\Controllers;

use App\Imports\TabelBsImport;
use App\Models\TabelB;
use Illuminate\Http\Request;
use DB;
use Exception;
use Maatwebsite\Excel\Facades\Excel;

class TabelBController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()) {
            $data = TabelB::query();
            return datatables($data)
                ->addIndexColumn()
                ->editColumn('action', 'datatables._action')
                ->rawColumns(['action'])
                ->toJson();
        }
        return view('table-b');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            TabelB::buat($request->all());
            DB::commit();
            $http_code = 200;
            $response = $this->save_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = TabelB::findOrFail($id);
        $data = [
            'id' => $data->id ?? null,
            'kode_toko' => $data->kode_toko ?? null,
            'nominal_transaksi' => $data->nominal_transaksi ?? null,
        ];
        $response = $this->show_detail_response($data);
        return response()->json($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            TabelB::ubah($request->all(),$id);
            DB::commit();
            $http_code = 200;
            $response = $this->update_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            TabelB::findOrFail($id)->delete();
            DB::commit();
            $http_code = 200;
            $response = $this->delete_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }
    public function import(Request $request){
        try {
            DB::beginTransaction();
            Excel::import(new TabelBsImport, $request->file('file'));
            $status = 'success';
            $msg = 'Import berhasil';
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $status = 'error';
            $msg = 'Import gagal, '.$e->getMessage();
        }
        return redirect()->back()->with($status, $msg);
    }
}
