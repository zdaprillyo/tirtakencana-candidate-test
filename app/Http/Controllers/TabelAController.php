<?php

namespace App\Http\Controllers;

use App\Imports\TabelAsImport;
use App\Models\TabelA;
use DB;
use Exception;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class TabelAController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()) {
            $data = TabelA::query();
            return datatables($data)
                ->addIndexColumn()
                ->editColumn('action', 'datatables._action')
                ->rawColumns(['action'])
                ->toJson();
        }
        return view('table-a');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            TabelA::buat($request->all());
            DB::commit();
            $http_code = 200;
            $response = $this->save_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TabelA  $tabelA
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tabelA = TabelA::whereKodeBaru($id)->firstOrFail();
        $data = [
            'id' => $tabelA->kode_baru ?? null,
            'kode_baru' => $tabelA->kode_baru ?? null,
            'kode_lama' => $tabelA->kode_lama ?? null,
        ];
        $response = $this->show_detail_response($data);
        return response()->json($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TabelA  $tabelA
     * @return \Illuminate\Http\Response
     */
    public function edit(TabelA $tabelA)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TabelA  $tabelA
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            TabelA::ubah($request->all(),$id);
            DB::commit();
            $http_code = 200;
            $response = $this->update_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TabelA  $tabelA
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            TabelA::whereKodeBaru($id)->delete();
            DB::commit();
            $http_code = 200;
            $response = $this->delete_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }

    public function import(Request $request){
        try {
            DB::beginTransaction();
            Excel::import(new TabelAsImport, $request->file('file'));
            $status = 'success';
            $msg = 'Import berhasil';
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $status = 'error';
            $msg = 'Import gagal, '.$e->getMessage();
        }
        return redirect()->back()->with($status, $msg);
    }
}
