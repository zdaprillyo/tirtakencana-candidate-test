<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TabelD extends Model
{
    use HasFactory;
    protected $table = 'tabel_d';
    protected $fillable = [
        'kode_sales','nama_sales'
    ];
    private static function fetch($args){
        return [
            'kode_sales' => $args->add_kode_sales ?? $args->edit_kode_sales,
            'nama_sales' => $args->add_nama_sales ?? $args->edit_nama_sales
        ];
    }

    public static function buat($params){
        $request = self::fetch((object)$params);
        return self::create($request);
    }

    public static function ubah($params,$id){
        $request = self::fetch((object)$params);
        return self::findOrFail($id)->update($request);
    }
}
