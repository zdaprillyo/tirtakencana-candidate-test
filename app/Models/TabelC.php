<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TabelC extends Model
{
    use HasFactory;
    protected $table = 'tabel_c';
    protected $fillable = [
        'kode_toko','area_sales'
    ];
    private static function fetch($args){
        return [
            'kode_toko' => $args->add_kode_toko ?? $args->edit_kode_toko,
            'area_sales' => $args->add_area_sales ?? $args->edit_area_sales
        ];
    }

    public static function buat($params){
        $request = self::fetch((object)$params);
        return self::create($request);
    }

    public static function ubah($params,$id){
        $request = self::fetch((object)$params);
        return self::findOrFail($id)->update($request);
    }
}
