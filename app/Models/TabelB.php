<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TabelB extends Model
{
    use HasFactory;
    protected $table = 'tabel_b';
    protected $fillable = [
        'kode_toko','nominal_transaksi'
    ];
    private static function fetch($args){
        return [
            'kode_toko' => $args->add_kode_toko ?? $args->edit_kode_toko,
            'nominal_transaksi' => $args->add_nominal_transaksi ?? $args->edit_nominal_transaksi
        ];
    }

    public static function buat($params){
        $request = self::fetch((object)$params);
        return self::create($request);
    }

    public static function ubah($params,$id){
        $request = self::fetch((object)$params);
        return self::findOrFail($id)->update($request);
    }
}
