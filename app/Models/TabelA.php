<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class TabelA extends Model
{
    use HasFactory;
    protected $table = 'tabel_a';
    protected $fillable = [
        'kode_baru','kode_lama'
    ];

    private static function fetch($args){
        return [
            'kode_lama' => $args->add_kode_lama ?? $args->edit_kode_lama ?? null
        ];
    }

    public static function buat($params){
        $request = self::fetch((object)$params);
        return self::create($request);
    }

    public static function ubah($params,$kode_baru){
        $request = self::fetch((object)$params);
        return self::whereKodeBaru($kode_baru)->update($request);
    }
}
