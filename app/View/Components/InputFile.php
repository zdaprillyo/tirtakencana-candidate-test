<?php

namespace App\View\Components;

use Illuminate\View\Component;

class InputFile extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $id,
        private string $name,
        private string $height = "200",
        private ?string $label = NULL,
        private ?string $helper = NULL,
        private ?string $value = NULL,
        private ?string $accept = NULL,
        private ?string $requiredMarkOnly = NULL,
        private ?string $required = NULL,
        private ?string $disabled = NULL,
        private ?string $single = NULL,
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.input-file', [
            'id' => $this->id,
            'label' => $this->label,
            'helper' => $this->helper,
            'name' => $this->name,
            'height' => $this->height,
            'value' => $this->value,
            'accept' => $this->accept,
            'requiredMarkOnly' => $this->requiredMarkOnly,
            'required' => $this->required,
            'disabled' => $this->disabled,
            'single' => $this->single,
        ]);
    }
}
