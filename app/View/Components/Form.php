<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Form extends Component
{
    private $action;
    private $method;
    private $submitIcon;
    private $submitType;
    private $sendFile;
    private $submit;
    private $submitSmall;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($action,$method,$submitIcon = 'fa fa-solid fa-check',$submitType = 'success',$sendFile = null,$submit = null,$submitSmall = null )
    {
        $this->action = $action;
        $this->method = $method;
        $this->submitIcon = $submitIcon;
        $this->submitType = $submitType;
        $this->sendFile = $sendFile;
        $this->submit = $submit;
        $this->submitSmall = $submitSmall;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $data = [
            'action' => $this->action,
            'method' => $this->method,
            'submitIcon' => $this->submitIcon,
            'submitType' => $this->submitType,
            'sendFile' => $this->sendFile,
            'submit' => $this->submit,
            'submitSmall' => $this->submitSmall
        ];
        return view('components.form',$data);
    }
}
