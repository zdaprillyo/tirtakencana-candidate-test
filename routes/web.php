<?php

use App\Http\Controllers\TabelAController;
use App\Http\Controllers\TabelBController;
use App\Http\Controllers\TabelCCOntroller;
use App\Http\Controllers\TabelDController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::resource('tabel-a',TabelAController::class);
Route::post('tabel-a/import', [TabelAController::class,'import'])->name('tabel-a.import');

Route::resource('tabel-b',TabelBController::class);
Route::post('tabel-b/import', [TabelBController::class,'import'])->name('tabel-b.import');

Route::resource('tabel-c',TabelCCOntroller::class);
Route::post('tabel-c/import', [TabelCController::class,'import'])->name('tabel-c.import');

Route::resource('tabel-d',TabelDController::class);
Route::post('tabel-d/import', [TabelDController::class,'import'])->name('tabel-d.import');


