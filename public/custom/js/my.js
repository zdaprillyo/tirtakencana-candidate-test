//general variabel
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    showCloseButton: true,
    timer: 5000,
    iconColor: 'white',
    customClass: {
        popup: 'colored-toast'
    },
    showClass: {
        popup: "bounceIn"
    },
    hideClass: {
        popup: "bounceOut"
    },
    didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
})
const DeleteConfirm = Swal.mixin({
    customClass: {
        confirmButton: 'btn btn-danger btn-icon-text',
        cancelButton: 'btn btn-success btn-icon-text mr-2',
        title: 'confirm-title',
        popup: 'confirm-toast'
    },
    buttonsStyling: false,
    reverseButtons: true,
    showCancelButton: true,
    icon: 'question',
    confirmButtonText: "<i class='fa fa-check btn-icon-text mr-2'></i>Ya",
    cancelButtonText: "<i class='fa fa-close btn-icon-text mr-2'></i>Tidak"
})
var global_update_url = null

//general function
function datatable_translate(attr, verb = 'ditambahkan') {
    return {
        "lengthMenu": "Menampilkan _MENU_ " + attr.toLowerCase() + " per halaman",
        "emptyTable": "Masih belum ada " + attr.toLowerCase() + " yang " + verb + ".",
        "zeroRecords": attr + " yang dicari tidak ditemukan.",
        "info": "Halaman _PAGE_ dari _PAGES_",
        "infoEmpty": "Tidak ada " + attr.toLowerCase() + ".",
        "infoFiltered": "(disaring dari total _MAX_ " + attr.toLowerCase() + ")",
        "loadingRecords": "Sedang memuat ...",
        "processing": "Sedang memproses ...",
        "search": "Cari:",
        "thousands": ".",
        "paginate": {
            "next": ">",
            "previous": "<"
        },
    }
}

function generate_store_listener(form_data_id,url,table = null,redirect = null){
     $("#"+form_data_id).on('submit', function(e){
        e.preventDefault()
        loading_screen()
        let form = $(this)
        let data = new FormData(form[0]);
        ajax_post_request(url,data,table,redirect)
    });
}

function generate_update_listener(form_data_id,table = null,redirect = null){
    $("#"+form_data_id).on('submit', function(e){
        e.preventDefault()
        loading_screen()
        let form = $(this)
        let data = new FormData(form[0]);
        ajax_post_request(global_update_url,data,table,redirect)
    });
}

function generate_show_listener(url,using_ajax_get_request = 0) {
    $(document).on('click', '.btn-edit', function(){
        loading_screen()
        let button = $(this)
        let id = button.data('id')
        global_update_url = url.replace('-id-', id);
        if(using_ajax_get_request){
            ajax_get_request(url.replace('-id-', id))
        }else{
            let data = {
                name : button.data('name')
            }
            show_detail(data)
        }
        Swal.close()
    })
}

function generate_delete_listener(url,table){
     $(document).on('click', '.btn-delete', function(){
        let button = $(this)
        let id = button.data('id')
        let name = button.data('name')

        DeleteConfirm.fire({
            title: "Yakin ingin menghapus " + name + " ?",
        }).then((result) => {
            if (result.isConfirmed) {
                let data = new FormData($("#form_delete")[0]);
                loading_screen()
                ajax_post_request(url.replace('-id-', id),data,table)
            }
        })
    })
}

function generate_datatable(title, url, columns, orders = [0, 'asc'],print_colums = [0]){
    let column = [{ data: 'DT_RowIndex', orderable: false, searchable: false, className: "text-center"}]
    columns.forEach(e => {
        column.push(e)
    });
    column.push({ data: 'action', orderable: false, searchable: false, className: "text-center"})
    return $('#datatable').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                title: title,
                extend: 'excelHtml5',
                exportOptions: {
                    columns: print_colums
                }
            },
            {
                title: title,
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: print_colums
                }
            },
        ],
        processing: true,
        serverSide: true,
        ajax: url,
        order: [
            orders
        ],
        columns: column,
        language: datatable_translate(title),
        drawCallback: function() {
            $('.fancybox').fancybox()
        },
    })
}

function loading_screen(){
    Swal.fire({
        didOpen: () => {
          Swal.showLoading()
        },
        background: 'rgba(255, 255, 255, 0)',
        allowOutsideClick: false,
        allowEscapeKey: false,
        showConfirmButton: false,
    });
}

function ajax_post_request(url,data,datatable = null,redirect = null){
    $.ajax({
        type: 'post',
        url: url,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 800000,
        success: function(result) {
            if (result.success) {
                $('.modal').modal('hide')
                $('.form-control').val('')
                Toast.fire({
                    icon: 'success',
                    title: result.message
                })
                if(datatable){
                    datatable.ajax.reload()
                }
                if(redirect){
                    window.setTimeout( function(){
                        window.location.replace(redirect);
                    }, 800);
                }
            }
            else {
                Toast.fire({
                    icon: 'error',
                    title: result.message,
                    width: '38%',
                })
            }
        },
        error: function (request, status, error) {
            ajax_error_response(request, status, error)
        }
    })
}

function ajax_get_request(url){
    $.ajax({
        type: 'get',
        url: url,
        processData: false,
        contentType: false,
        timeout: 800000,
        success: function(result) {
            if (result.success) {
                show_detail(result.data)
            }
        },
        error: function (request, status, error) {
            ajax_error_response(request, status, error)
        }
    })
}
function ajax_post_request_for_detail(url,params){
    $.ajax({
        type: 'post',
        url: url,
        data: params,
        success: function(result) {
            detail_received(result)
        },
        error: function (request, status, error) {
            ajax_error_response(request, status, error)
        }
    })
}
function ajax_error_response(request, status, error){
    let title = "Terjadi masalah pada server!!"
    let msg = "- Harap muat ulang halaman dan coba lagi<br>- Jika masalah terus terjadi harap hubungi penyedia layanan anda!"
    let icon = 'error';
    if (request.status==422) {
        title = "Kesalahan validasi"
        let validation_error_msg="";
        if(request.responseJSON.errors){
            Object.keys(request.responseJSON.errors).forEach(key => {
                validation_error_msg += "- "+request.responseJSON.errors[key]+"<br>"
            });
        }else{
            validation_error_msg = "- "+request.responseJSON.error+"<br>"+msg
        }

        msg = validation_error_msg
        icon = 'warning'
    }
    Toast.fire({
        icon: icon,
        title: title,
        html: msg,
        width: '38%',
    })
}

function generate_datetimepicker(selector,format) {
    return $(selector).datetimepicker({
        format: format,
        icons: {
                time: 'mdi mdi-calendar-clock',
                date: 'mdi mdi-calendar',
                up: 'mdi mdi-chevron-up',
                down: 'mdi mdi-chevron-down',
                previous: 'mdi mdi-chevron-left',
                next: 'mdi mdi-chevron-right',
                today: 'mdi mdi-calendar-check',
                clear: 'mdi mdi-delete',
                close: 'fa fa-times'
            }
        });
}

function generate_icon_listener(id){
    $(document).on('keyup', '#'+id, function(){
        let icon = $('#'+id).val()
        console.log('icon',icon,'id','#'+id+'_preview');
        $('#'+id+'_preview').attr('class',icon)
    })
}

function formatNumber(value) {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function generate_number_formater(selector){
    $(document).on('keyup', selector, function(){
        $(this).val(formatNumber($(this).val()))
    })
}

function generate_chart(selector,labels,dataset_label,dataset_data){
    return new Chart($("#"+selector), {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: dataset_label,
                type: 'line',
                data: dataset_data,
                backgroundColor: "rgba(3, 208, 234, 0.23)",
                borderColor: "#03d0ea",
                pointBackgroundColor:'transparent',
                pointHoverBackgroundColor:'#03d0ea',
                pointBorderWidth :2,
                pointRadius :3,
                pointHoverRadius :3,
                borderWidth: 3
            }]
        },
        options: {
            maintainAspectRatio: false,
            legend: {
                display: false,
                labels: {
                    fontColor: '#585757',
                    boxWidth:40
                }
            },
            tooltips: {
                displayColors:false
            },
            scales: {
                xAxes: [{
                    barPercentage: .3,
                    ticks: {
                        beginAtZero:true,
                        fontColor: '#585757'
                    },
                    gridLines: {
                        display: true ,
                        color: "rgba(0, 0, 0, 0.05)"
                    },
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero:true,
                        fontColor: '#585757'
                    },
                    gridLines: {
                        display: true ,
                        color: "rgba(0, 0, 0, 0.05)"
                    },
                }]
            }
        }
    });
}

